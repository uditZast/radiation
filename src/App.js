import React, { Fragment, Component } from "react";
import "./App.scss";
import Routes from "./routes";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Routes />
      </Fragment>
    );
  }
}

export default App;
