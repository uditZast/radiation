import React, { Component, Fragment } from "react";

class NotFound extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Fragment>
        <div>404 Not Found</div>
      </Fragment>
    );
  }
}

export default NotFound;
