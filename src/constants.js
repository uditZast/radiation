export const PATH = {
  HOME: "/",
  PRODUCTS: "/products",
  SERVICES: "/services",
  CONTACT_US: "/contact-us",
  ABOUT_US: "/about-us"
};
