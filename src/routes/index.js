import React, { Component, Fragment } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Home from "../components/home";
import AboutUs from "../components/aboutUs";
import Services from "../components/services";
import NotFound from "../components/NotFound";
import { PATH } from "../constants";
import "./loader.scss";
import "./StickyNavigation";

const Loader = () => (
  <div className="circle-container">
    <div className="circle-content" />
    <div className="animated-block first" />
    <div className="animated-block second" />
  </div>
);

class Routes extends Component {
  render() {
    return (
      <Fragment>
        <section className="et-hero-tabs">
          <h1>STICKY SLIDER NAV</h1>
          <h3>Sliding content with sticky tab nav</h3>
          <div className="et-hero-tabs-container">
            <a className="et-hero-tab" href="#tab-es6">
              ES6
            </a>
            <a className="et-hero-tab" href="#tab-flexbox">
              Flexbox
            </a>
            <a className="et-hero-tab" href="#tab-react">
              React
            </a>
            <a className="et-hero-tab" href="#tab-angular">
              Angular
            </a>
            <a className="et-hero-tab" href="#tab-other">
              Other
            </a>
            <span className="et-hero-tab-slider"></span>
          </div>
        </section>

        <main className="et-main">
          <section className="et-slide" id="tab-es6">
            <h1>ES6</h1>
            <h3>something about es6</h3>
          </section>
          <section className="et-slide" id="tab-flexbox">
            <h1>Flexbox</h1>
            <h3>something about flexbox</h3>
          </section>
          <section className="et-slide" id="tab-react">
            <h1>React</h1>
            <h3>something about react</h3>
          </section>
          <section className="et-slide" id="tab-angular">
            <h1>Angular</h1>
            <h3>something about angular</h3>
          </section>
          <section className="et-slide" id="tab-other">
            <h1>Other</h1>
            <h3>something about other</h3>
          </section>
        </main>
        <BrowserRouter>
          <Switch>
            <Route exact path={PATH.HOME} component={Home} />
            <Route path={PATH.ABOUT_US} component={AboutUs} />
            <Route path={PATH.SERVICES} component={Services} />
            <Route path="*" component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Fragment>
    );
  }
}

export default Routes;
